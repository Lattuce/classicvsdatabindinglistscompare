package fm.last.wathcer.lastfmsongswatcher.domain

import com.nhaarman.mockito_kotlin.whenever
import fm.last.wathcer.lastfmsongswatcher.data.TrackInfo
import fm.last.wathcer.lastfmsongswatcher.utils.indexingTracks
import fm.last.wathcer.lastfmsongswatcher.utils.multiplyTracks
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

/**
 * Тест на класс {@link BaseInteractor}
 *
 * @author Leonid Emelyanov on 22/11/2017.
 */
@RunWith(MockitoJUnitRunner.Silent::class)
class BaseInteractorTest {
    private lateinit var interactor: BaseInteractor

    @Mock
    private lateinit var repository: Repository

    @Before
    fun setUp() {
        interactor = BaseInteractor(repository)
    }

    @Test
    fun getTracksTest() {
        val tracks = getTracks()

        whenever(repository.getTracks()).thenReturn(Single.just(tracks))
        assertEquals(interactor.getTracks().blockingGet(), getExpectedTracks(tracks))
    }

    private fun getTracks(): List<TrackInfo> {
        val list = ArrayList<TrackInfo>()

        for (i in 1..100) {
            list += TrackInfo(name = "test")
        }
        return list
    }

    private fun getExpectedTracks(tracks: List<TrackInfo>)
            = tracks.multiplyTracks(100).indexingTracks()
}