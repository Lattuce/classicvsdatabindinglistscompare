package fm.last.wathcer.lastfmsongswatcher.view.modern

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.View
import com.android.databinding.library.baseAdapters.BR
import fm.last.wathcer.lastfmsongswatcher.data.TrackInfo

/**
 * View holder с data bindings
 *
 * @author Leonid Emelyanov on 10/11/2017.
 */
class ModernViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val binding: ViewDataBinding? = DataBindingUtil.getBinding(itemView)

    fun setViewModel(viewModel: TrackInfo) {
        binding?.setVariable(BR.viewModel, viewModel)
        binding?.executePendingBindings()
    }
}