package fm.last.wathcer.lastfmsongswatcher.domain

import fm.last.wathcer.lastfmsongswatcher.data.TrackInfo
import io.reactivex.Single

/**
 * Интерфейс репозитория
 *
 * @author Leonid Emelyanov on 12/11/2017.
 */
interface Repository {
    fun getTracks(): Single<List<TrackInfo>>
}