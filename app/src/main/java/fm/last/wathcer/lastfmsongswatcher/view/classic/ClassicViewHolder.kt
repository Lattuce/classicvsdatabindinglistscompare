package fm.last.wathcer.lastfmsongswatcher.view.classic

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import fm.last.wathcer.lastfmsongswatcher.R
import fm.last.wathcer.lastfmsongswatcher.data.Image
import fm.last.wathcer.lastfmsongswatcher.data.TrackInfo

/**
 * Классический view holder для отображения списка песен
 *
 * @author Leonid Emelyanov on 07/11/2017.
 */
class ClassicViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val numberTextView: TextView = itemView.findViewById(R.id.number_text_view)
    private val songNameTextView: TextView = itemView.findViewById(R.id.song_name_text_view)
    private val albumNameTextView: TextView = itemView.findViewById(R.id.album_name_text_view)
    private val durationTextView: TextView = itemView.findViewById(R.id.song_duration_text_view)
    private val albumCoverImageView: ImageView = itemView.findViewById(R.id.album_cover_image_view)

    fun bind(track: TrackInfo?) {
        numberTextView.text = track?.position?.toString()
        songNameTextView.text = track?.name
        albumNameTextView.text = track?.getAlbumName()

        durationTextView.text = track?.getDurationTime()
        durationTextView.visibility =
                if (track?.getDurationMills() ?: 0 > 0) {
                    View.VISIBLE
                } else {
                    View.GONE
                }
        track?.url?.let {
            Glide.with(itemView.context)
                    .load(track.getImageUrl(Image.SIZE_LARGE))
                    .into(albumCoverImageView)
        }
    }
}