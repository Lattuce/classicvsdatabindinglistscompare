package fm.last.wathcer.lastfmsongswatcher.utils

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import fm.last.wathcer.lastfmsongswatcher.data.TrackInfo

/**
 * Расширения
 *
 * @author Leonid Emelyanov on 07/11/2017.
 */
fun ViewGroup.inflate(@LayoutRes resId: Int): View =
        LayoutInflater.from(this.context).inflate(resId, this, false)

fun ViewGroup.inflateBinding(@LayoutRes resId: Int): ViewDataBinding =
        DataBindingUtil.inflate(LayoutInflater.from(this.context), resId, this, false)

fun List<TrackInfo>.multiplyTracks(count: Int): List<TrackInfo> {
    val list = ArrayList<TrackInfo>()

    for (i in 1..count) {
        this.forEach {
            list += it.copy()
        }
    }
    return list
}

fun List<TrackInfo>.indexingTracks(): List<TrackInfo> {
    this.forEachIndexed { index, trackInfo -> trackInfo.position = index + 1 }
    return this
}