package fm.last.wathcer.lastfmsongswatcher.data

import fm.last.wathcer.lastfmsongswatcher.kodein.KodeinInstance
import io.reactivex.Single
import retrofit2.http.GET

/**
 * Интерфейс получения данных с Last.FM
 *
 * @author Leonid Emelyanov on 08.10.17.
 */
interface LastFmApi {
    @GET("/2.0/?method=chart.gettoptracks&api_key=${KodeinInstance.API_KEY}&format=json&limit=100")
    fun getTopTracks(): Single<Tracks>
}