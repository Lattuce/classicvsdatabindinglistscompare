package fm.last.wathcer.lastfmsongswatcher.domain

import com.github.salomonbrys.kodein.instance
import fm.last.wathcer.lastfmsongswatcher.data.LastFmApi
import fm.last.wathcer.lastfmsongswatcher.data.TrackInfo
import fm.last.wathcer.lastfmsongswatcher.data.Tracks
import fm.last.wathcer.lastfmsongswatcher.kodein.KodeinInstance
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single

/**
 * Репозиторий
 *
 * @author Leonid Emelyanov on 12/11/2017.
 */
class RepositoryImpl : Repository {
    private var cachedItems: Tracks? = null
    private val connectable: Observable<Tracks> by lazy {
        Observable.concat(getCachedItems().toObservable(), api.getTopTracks().toObservable())
                .firstOrError()
                .toObservable()
                .replay()
                .autoConnect()
    }

    private val api: LastFmApi = KodeinInstance.kodein.instance()

    override fun getTracks(): Single<List<TrackInfo>> =
            connectable.firstOrError()
                    .doOnSuccess { items -> cachedItems = items }
                    .map { it.track?.track ?: emptyList() }

    private fun getCachedItems(): Maybe<Tracks> =
            if (cachedItems != null) {
                Maybe.just(cachedItems)
            } else {
                Maybe.empty()
            }
}