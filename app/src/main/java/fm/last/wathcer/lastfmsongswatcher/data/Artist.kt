package fm.last.wathcer.lastfmsongswatcher.data

import com.google.gson.annotations.SerializedName

/**
 * Artist
 *
 * @author Leonid Emelyanov on 09.10.17.
 */
data class Artist(
        @SerializedName("name")
        var name: String? = null,

        @SerializedName("mbid")
        var mbid: String? = null,

        @SerializedName("url")
        var url: String? = null
)