package fm.last.wathcer.lastfmsongswatcher.view

import android.support.v7.widget.RecyclerView
import fm.last.wathcer.lastfmsongswatcher.data.TrackInfo

/**
 * Базовый адаптер
 *
 * @author Leonid Emelyanov on 09/11/2017.
 */
abstract class BaseAdapter<T : RecyclerView.ViewHolder> : RecyclerView.Adapter<T>() {
    protected val items: MutableList<TrackInfo> = ArrayList()

    override fun getItemCount() = items.size

    fun setItems(items: List<TrackInfo>) {
        this.items.clear()
        this.items.addAll(items)

        notifyDataSetChanged()
    }
}