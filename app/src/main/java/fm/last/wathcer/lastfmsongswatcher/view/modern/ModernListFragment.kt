package fm.last.wathcer.lastfmsongswatcher.view.modern


import android.os.Bundle
import fm.last.wathcer.lastfmsongswatcher.data.TrackInfo
import fm.last.wathcer.lastfmsongswatcher.view.ListFragment


/**
 * Фрагмент списка на data bindings
 */
class ModernListFragment : ListFragment() {
    private lateinit var adapter: ModernAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = ModernAdapter()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        recycleView?.adapter = adapter
    }

    override fun onLoadingDone(items: List<TrackInfo>) {
        adapter.setItems(items)
    }
}
