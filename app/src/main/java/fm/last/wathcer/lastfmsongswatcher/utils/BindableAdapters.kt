package fm.last.wathcer.lastfmsongswatcher.utils

import android.databinding.BindingAdapter
import android.view.View
import android.widget.TextView
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import fm.last.wathcer.lastfmsongswatcher.data.Image

/**
 * Адаптеры для bindings
 *
 * @author Leonid Emelyanov on 10/11/2017.
 */
object BindableAdapters {

    @JvmStatic
    @BindingAdapter("imageUrl")
    fun imageUrl(view: CircleImageView, images: List<Image>?) {
        Glide.with(view.context)
                .load(images?.find { it.size?.equals(Image.SIZE_LARGE) ?: false }?.text)
                .into(view)
    }

    @JvmStatic
    @BindingAdapter("showDuration")
    fun showDuration(view: View, duration: Long?) {
        view.visibility =
                if (duration ?: 0 > 0) {
                    View.VISIBLE
                } else {
                    View.GONE
                }

    }

    @JvmStatic
    @BindingAdapter("intToString")
    fun intToString(view: TextView, int: Int) {
        view.text = int.toString()
    }
}