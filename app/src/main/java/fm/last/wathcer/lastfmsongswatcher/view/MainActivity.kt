package fm.last.wathcer.lastfmsongswatcher.view

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import fm.last.wathcer.lastfmsongswatcher.R
import fm.last.wathcer.lastfmsongswatcher.view.classic.ClassicListFragment
import fm.last.wathcer.lastfmsongswatcher.view.modern.ModernListFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var viewPager: ViewPager
    private lateinit var pagerAdapter: SwipeAdapter

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_classic_list -> {
                viewPager.currentItem = 0
                true
            }
            R.id.navigation_moder_list -> {
                viewPager.currentItem = 1
                true
            }
            else -> false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        pagerAdapter = SwipeAdapter(supportFragmentManager)
        pagerAdapter.setItems(arrayListOf(
                ClassicListFragment(),
                ModernListFragment()
        ))

        viewPager = findViewById(R.id.view_pager)
        viewPager.adapter = pagerAdapter
        viewPager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> navigation.selectedItemId = R.id.navigation_classic_list
                    1 -> navigation.selectedItemId = R.id.navigation_moder_list
                }
            }
        })
    }
}