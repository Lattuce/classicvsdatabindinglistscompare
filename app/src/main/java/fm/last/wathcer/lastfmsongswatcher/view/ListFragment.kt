package fm.last.wathcer.lastfmsongswatcher.view


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.salomonbrys.kodein.instance
import fm.last.wathcer.lastfmsongswatcher.R
import fm.last.wathcer.lastfmsongswatcher.data.TrackInfo
import fm.last.wathcer.lastfmsongswatcher.domain.BaseInteractor
import fm.last.wathcer.lastfmsongswatcher.kodein.KodeinInstance
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Фрагмент для списка треков
 */
abstract class ListFragment : Fragment() {
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    protected var recycleView: RecyclerView? = null

    private val interactor: BaseInteractor = KodeinInstance.kodein.instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false)

        recycleView = view?.findViewById(R.id.tracks_recycle_view)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout)
        swipeRefreshLayout?.setOnRefreshListener { updateData() }

        return view
    }

    override fun onResume() {
        super.onResume()
        updateData()
    }

    private fun updateData() {
        swipeRefreshLayout?.isRefreshing = true
        interactor.getTracks()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { result ->
                    swipeRefreshLayout?.isRefreshing = false
                    onLoadingDone(result)
                }
    }

    protected abstract fun onLoadingDone(items: List<TrackInfo>)
}