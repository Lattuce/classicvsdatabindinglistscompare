package fm.last.wathcer.lastfmsongswatcher.view

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

/**
 * Адаптер для переключения между фрагментами
 *
 * @author Leonid Emelyanov on 12/11/2017.
 */
class SwipeAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {
    private val items: MutableList<Fragment> = ArrayList()

    override fun getItem(position: Int): Fragment = items[position]

    override fun getCount(): Int = items.size

    fun setItems(items: List<Fragment>) {
        this.items.clear()
        this.items.addAll(items)
    }
}