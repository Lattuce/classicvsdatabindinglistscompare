package fm.last.wathcer.lastfmsongswatcher.data

import com.google.gson.annotations.SerializedName

/**
 * Tracks
 *
 * @author Leonid Emelyanov on 07/11/2017.
 */
data class Tracks(

        @SerializedName("tracks")
        var track: Track? = null
)