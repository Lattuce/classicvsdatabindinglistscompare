package fm.last.wathcer.lastfmsongswatcher.data

import com.google.gson.annotations.SerializedName

/**
 * Image
 *
 * @author Leonid Emelyanov on 09.10.17.
 */
class Image(

        @SerializedName("#text")
        var text: String? = null,

        @SerializedName("size")
        var size: String? = null
) {
    companion object {
        val SIZE_SMALL = "small"
        val SIZE_MEDIUM = "medium"
        val SIZE_LARGE = "large"
        val SIZE_EXTRALARGE = "extralarge"
    }
}