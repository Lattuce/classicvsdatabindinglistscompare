package fm.last.wathcer.lastfmsongswatcher.domain

import fm.last.wathcer.lastfmsongswatcher.data.TrackInfo
import fm.last.wathcer.lastfmsongswatcher.utils.indexingTracks
import fm.last.wathcer.lastfmsongswatcher.utils.multiplyTracks
import io.reactivex.Single

/**
 * Базовый интерактор
 *
 * @author Leonid Emelyanov on 07.10.17.
 */
class BaseInteractor(private val repository: Repository) {

    fun getTracks(): Single<List<TrackInfo>> =
            repository.getTracks().map { it.multiplyTracks(100).indexingTracks() }
}