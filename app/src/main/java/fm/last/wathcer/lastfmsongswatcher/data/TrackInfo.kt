package fm.last.wathcer.lastfmsongswatcher.data

import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.util.*

/**
 * Track info
 *
 * @author Leonid Emelyanov on 09.10.17.
 */
data class TrackInfo(
        @SerializedName("name")
        var name: String? = null,

        @SerializedName("duration")
        var duration: String? = null,

        @SerializedName("playcount")
        var playcount: String? = null,

        @SerializedName("listeners")
        var listeners: String? = null,

        @SerializedName("mbid")
        var mbid: String? = null,

        @SerializedName("url")
        var url: String? = null,

        @SerializedName("streamable")
        var streamable: Streamable? = null,

        @SerializedName("artist")
        var artist: Artist? = null,

        @SerializedName("image")
        var image: List<Image>? = null,

        var position: Int = 0
) {
    fun getDurationMills(): Long = (duration?.toInt() ?: 0) * 1000L

    fun getDurationTime(): String = timeFormat.format(Date(getDurationMills()))

    fun getAlbumName() = artist?.name

    fun getImageUrl(size: String) = image?.find { it.size?.equals(size) ?: false }?.text

    companion object {
        val timeFormat by lazy { SimpleDateFormat("m:ss", Locale.getDefault()) }
    }
}