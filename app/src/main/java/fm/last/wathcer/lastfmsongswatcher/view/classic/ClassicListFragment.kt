package fm.last.wathcer.lastfmsongswatcher.view.classic


import android.os.Bundle
import fm.last.wathcer.lastfmsongswatcher.data.TrackInfo
import fm.last.wathcer.lastfmsongswatcher.view.ListFragment


/**
 * Фрагмент для классического списка
 */
class ClassicListFragment : ListFragment() {
    private lateinit var adapter: ClassicAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = ClassicAdapter()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        recycleView?.adapter = adapter
    }

    override fun onLoadingDone(items: List<TrackInfo>) {
        adapter.setItems(items)
    }
}
