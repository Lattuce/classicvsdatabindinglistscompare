package fm.last.wathcer.lastfmsongswatcher.kodein

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.singleton
import fm.last.wathcer.lastfmsongswatcher.data.LastFmApi
import fm.last.wathcer.lastfmsongswatcher.domain.BaseInteractor
import fm.last.wathcer.lastfmsongswatcher.domain.Repository
import fm.last.wathcer.lastfmsongswatcher.domain.RepositoryImpl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Kodein
 *
 * @author Leonid Emelyanov on 04/12/2017.
 */
class KodeinInstance {
    companion object {
        private const val BASE_URL = "http://ws.audioscrobbler.com/"
        const val API_KEY = "21f8ad6ea26baa5bfc195f0e5a35489c"

        val kodein = Kodein {
            bind<LastFmApi>() with singleton { getRetrofit().create(LastFmApi::class.java) }
            bind<Repository>() with singleton { RepositoryImpl() }
            bind<BaseInteractor>() with singleton { BaseInteractor(kodein.instance()) }
        }

        private fun getClient() = OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
                .build()

        private fun getRetrofit() = Retrofit.Builder()
                .client(getClient())
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }
}