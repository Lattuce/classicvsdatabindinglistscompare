package fm.last.wathcer.lastfmsongswatcher.view.classic

import android.view.ViewGroup
import fm.last.wathcer.lastfmsongswatcher.R
import fm.last.wathcer.lastfmsongswatcher.utils.inflate
import fm.last.wathcer.lastfmsongswatcher.view.BaseAdapter

/**
 * Адаптер для классического списка
 *
 * @author Leonid Emelyanov on 07/11/2017.
 */
class ClassicAdapter : BaseAdapter<ClassicViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClassicViewHolder =
            ClassicViewHolder(parent.inflate(R.layout.classic_item_list))

    override fun onBindViewHolder(holder: ClassicViewHolder, position: Int) {
        holder.bind(items[position])
    }
}