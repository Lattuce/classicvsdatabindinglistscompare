package fm.last.wathcer.lastfmsongswatcher.data

import com.google.gson.annotations.SerializedName

/**
 * Tracks container
 *
 * @author Leonid Emelyanov on 09.10.17.
 */
data class Track(
        @SerializedName("track")
        var track: List<TrackInfo> = emptyList()
)