package fm.last.wathcer.lastfmsongswatcher.view.modern

import android.view.ViewGroup
import fm.last.wathcer.lastfmsongswatcher.R
import fm.last.wathcer.lastfmsongswatcher.utils.inflateBinding
import fm.last.wathcer.lastfmsongswatcher.view.BaseAdapter

/**
 * Адаптер для data binding
 *
 * @author Leonid Emelyanov on 10/11/2017.
 */
class ModernAdapter : BaseAdapter<ModernViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ModernViewHolder(parent.inflateBinding(R.layout.modern_item_list).root)

    override fun onBindViewHolder(holder: ModernViewHolder, position: Int) {
        holder.setViewModel(items[position])
    }
}