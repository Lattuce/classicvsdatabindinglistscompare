package fm.last.wathcer.lastfmsongswatcher.data

import com.google.gson.annotations.SerializedName

/**
 * Streamable
 *
 * @author Leonid Emelyanov on 09.10.17.
 */
data class Streamable(

        @SerializedName("#text")
        var text: String? = null,

        @SerializedName("fulltrack")
        var fullTrack: String? = null
)